String.prototype.hex2bin = function (h) {
    return this.split('').reduce(function(acc, i) {
        return acc + ('000' + parseInt(i, 16).toString(2)).substr(-4, 4);
    }, '')
}

var hexa_module = {'01' : 1, '02' : 2, '04' : 3, '08' : 4, '10' : 5};

socket.on('detecteurTrame', function(data) {
	console.log("getInfoEquipement");
	
	animateListeDetecteurs(detecteurHexToId(data));
});

/* A partir d'une trame série, renvoie un tableau des id
 * de tous les détecteurs qui ont un état différent de 00 (aucune détection) */
var detecteurHexToId = function(data)
{
	// [1] : Module [2] : Message
	var infoDetecteur = (/^608(\d{2})\s(\S{1,2})/.exec(data));
	
	if (infoDetecteur !== null)
	{		
		// Préparation de la première moitié de l'id
		var id = "#d_" + hexa_module[infoDetecteur[1]] + "_";
		var listeDetecteurs = [];
		
		// Conversion de la partie message (hexa) en binaire
		var binMsg = infoDetecteur[2].hex2bin();
		
		// Parcours du message binaire par segments de deux chiffres
		for (s = 0; s < binMsg.length; s+=2)
		{
			// Extraction du code binaire pour le segment parcouru
			segCode = binMsg.substr(s, 2);
			
			console.log("Segment " + s + " : " + segCode);
			
			if (segCode != "00")
			{
				/* Pour chaque détecteur, on crée une nouvelle ligne dans le tableau
				 * listeDetecteurs ayant pour clé l'id complet du détecteur (ex : #d_3_1)
				 * dans laquette on stocke le code binaire qui a été renvoyé par le détecteur */
				listeDetecteurs[id + ((binMsg.length - s) / 2)] = segCode;
			}
		}
		
		/* Exemple de tableau retourné :
		* {
		*    "#d_3_1" : "01",
		*    "#d_3_3" : "11"
		* } 
		*/
		return listeDetecteurs;
	}
	else return null;
}

var animateListeDetecteurs = function(liste)
{	
	for (var key in liste)
	{
		if (liste[key] == 10)
			animateDetecteur($(key));
	}
}

var animateDetecteur = function($target)
{
	// Arrêt des animations précédentes
	$target.finish();
	
	originalRadius = $target.attr("r");
	
	$target.css("stroke", "#000000");
	$target.animate({ opacity : 0, r : 100 });
	$target.animate({ opacity : 1, r : originalRadius }, 0, "swing",
	function() {
		$target.css("stroke", "none");
	});
}


/*
	//var dataDetecteur = '60802 08';

	var readLengthEigth = function(dataDetecteur)
	{
		
	
		var regDetecteur = (/608(\d{2})\s((\d|[A-F]){2})/.exec(dataDetecteur));

		var regModule = regDetecteur[1];

		console.log(" mod " + hexa_module[regModule]);

		console.log("hexa :" + regDetecteur[2]);

		var requete = regDetecteur[2].hex2bin();

		console.log("binaire :" + requete);

		var regCab = (/(\d{2})(\d{2})(\d{2})(\d{2})/.exec(requete));

		console.log("avant " + regCab);

		var regCabReverse = regCab.reverse();
			
		console.log("apres " + regCabReverse);	

		var regCab1 = regCab[1],
			regCab2 = regCab[2],
			regCab3 = regCab[3],
			regCab4 = regCab[4];

		console.log("cab4  : " + regCab4);
		console.log("cab3  : " + regCab3 );
		console.log("cab2  : " + regCab2);
		console.log("cab1  : " + regCab1);

		for (var i = 0; i <= 3 ; i++) {

				console.log( " cab " + regCabReverse[i]);

			if (regCabReverse[i] != '00'){

				console.log('#d_'+ hexa_module[regModule] +'_' + (i + 1) );
				
				animateDetecteur($('#d_'+ hexa_module[regModule] +'_' + (i + 1)));

			}

		}
	}





var  readLengthSeven = function (dataDetecteur){


		
		var regDetecteur = (/608(\d{2})\s((\d|[A-F]))/.exec(dataDetecteur));

		var regModule = regDetecteur[1];

		console.log(" mod " + hexa_module[regModule]);

		var newDetecteur = ("0"+ regDetecteur[2]);

		console.log("concat: "+ newDetecteur);
		console.log("hexa :" + regDetecteur[2] );

		var requete = newDetecteur.hex2bin();

		console.log("binaire :" + requete);

		var regCab = (/(\d{2})(\d{2})(\d{2})(\d{2})/.exec(requete));

		console.log("avant " + regCab);

		var regCabReverse = regCab.reverse();
			
		console.log("apres " + regCabReverse);	

		var regCab1 = regCab[1],
			regCab2 = regCab[2],
			regCab3 = regCab[3],
			regCab4 = regCab[4];

		console.log("cab4  : " + regCab4);
		console.log("cab3  : " + regCab3 );
		console.log("cab2  : " + regCab2);
		console.log("cab1  : " + regCab1);

		for (var i = 0; i <= 3 ; i++) {

				console.log( " cab " + regCabReverse[i]);

			if (regCabReverse[i] != '00'){

				console.log('#d_'+ hexa_module[regModule] +'_' + (i + 1) );
				
				animateDetecteur($('#d_'+ hexa_module[regModule] +'_' + (i + 1)));

			}

		}



}

*/