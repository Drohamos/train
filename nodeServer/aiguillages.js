	
String.prototype.insertAt = function(index, string) 
{
	return ((index < 8)? this.substr(0, 8 - index) : "") + string + ((index > 2)? this.substr(8 - index + 2) : "");
}

// Convertit la chaine de caractères en binaire
String.prototype.bin2hex = function()
{	
	return this.match(/.{4}/g).reduce(function(acc, i) {
		return acc + parseInt(i, 2).toString(16);
	}, '');
}

$(document).ready(function()
{
	/*
	 * Arg : id de l'aiguillage (# compris), [forcePosition] : position à prendre
	 * Si forcePosition n'est pas initialisé, inverse la position de l'aiguillage,
	 * sinon, le met à la position indiquée (01 : tout droit, 10 : bifurcation/croisement)
	 */
	function rotate(arg, forcePosition)
	{
		a = typeof a !== 'undefined' ? forcePosition : false;
		
		console.log(arg);
		
		if (forcePosition == "10" || $(arg).css("transform") == "none")
		{
			$(arg).css({
				"transform-origin":"50% 50%",
				"transform": "rotate(90deg)"
			});
		}
		else if (forcePosition == "01" || true)
		{
			$(arg).css({
				"transform": "none"
			});	
		}
	}


	//------------       1     2    3    4
	var modules = ["00","01","02","04","08"];

	// @todo-loco : Vérifier exactitude pour croisements
	$("#signalisation > g").on("click", function(event) {

		var signal =  "#"+ event.currentTarget.id + " .signal";

		rotate(signal);

		var signalId = event.currentTarget.id;

		var aiguillage = (/sign_(\d)_(\d)/.exec(signalId));

		function getBinHex (bin){

			var requete = "00000000".insertAt(aiguillage[2] * 2,  bin).bin2hex();

			var serialMessage = "601" + modules[aiguillage[1]] + " " + requete 

			socket.emit("sendSerial", serialMessage);

			console.log(serialMessage );

		}
		
		if ($(signal).css("transform") == "none") {

			getBinHex("01");
			console.log("Tout droit")
			
		}
		else
		{
			getBinHex("10");
			console.log("Bifurcation");
		}
	
	});
});			












	 

