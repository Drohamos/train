/*
 *	Pilotage d'une maquette ferroviaire via serveur web Raspberry Pi
 *	         Projet 2ème année UHA 4.0, Janvier 2017
 *	        Équipe : Robin B. - Samir B. - Stéphane B.
 */

var http = require('http');
var fs = require('fs');

var serialEnabled   = true;
var serialInterface = "/dev/ttyUSB0";

/* SERVEUR HTTP */
var server = http.createServer(function(req, res) {
	console.log("HTTP : Request : " + req.url);
		
	switch (req.url)
	{
		case "/style.css" :
			fs.readFile('./style.css', 'utf-8', function(error, content) {
				res.writeHead(200, {"Content-Type": "text/css"});
				res.end(content);
			});
			break;
		case "/aiguillages.js" :
			fs.readFile('./aiguillages.js', 'utf-8', function(error, content) {
				res.writeHead(200, {"Content-Type": "text/javascript"});
				res.end(content);
			});
			break;
		case "/feux.js" :
			fs.readFile('./feux.js', 'utf-8', function(error, content) {
				res.writeHead(200, {"Content-Type": "text/javascript"});
				res.end(content);
			});
			break;
		case "/detecteurs.js" :
			fs.readFile('./detecteurs.js', 'utf-8', function(error, content) {
				res.writeHead(200, {"Content-Type": "text/javascript"});
				res.end(content);
			});
		case "": case "/": case "/index.html":
			fs.readFile('./index.html', 'utf-8', function(error, content) {
				res.writeHead(200, {"Content-Type": "text/html"});
				res.end(content);
			});
			break;
			
		// Dans tous les autres cas, erreur 404
		default:
			res.writeHead(404, {"Content-Type": "text/html"});
			res.end();
			break;
	}
});


/***********************/
/*** Connexion série ***/
/***********************/

if (serialEnabled == true)
{
	var serialport = require("serialport");
	var SerialPort = serialport.SerialPort;
	
	var sp = new SerialPort(serialInterface, {
		baudrate: 57600,
		parser: serialport.parsers.readline("\n")
	}, false);
	
	// Ouverture de la connexion série
	sp.open(function(error) {
		// En cas d'échec ouverture connexion -> désactivation données série
		if (error) {
			console.log("Erreur port série : " + error);
			serialEnabled = false;
			console.log("Connexion série désactivée jusqu'au redémarrage du serveur");
		}
		// Sinon, création du listener données série
		else {
			console.log('Connexion série ouverte');

			// On reçoit des données
			sp.on('data', function(data) {
				console.log('data received: ' + data);
				
				// Information reçue des détecteurs de passage
				io.sockets.emit("detecteurTrame", data);
			});
		}
	});
}


/*****************/
/*** Socket.io ***/
/*****************/

var io = require('socket.io').listen(server);
server.listen(3000);

console.log("HTTP/Socket : serveur démarré sur le port 3000");
if (!serialEnabled) console.log("Connexion série [OFF]");

// Quand un client se connecte, on le note dans la console
io.sockets.on('connection', function (socket) {
    console.log('Socket : Un client est connecté !');
	
	// A la demande de client, envoi de message série
	socket.on("sendSerial", function(message)
	{
		// On met le message en majuscules (pour l'héxa)
		message = message.toUpperCase();
		
		console.log("SerialWrite : " + message);
		
		if (serialEnabled == true)
			sp.write(message);
	});
});
