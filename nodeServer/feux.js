// Identifiants hexa. des modules
var module_hexa = ["00", "01", "02", "04", "08", "10"];

// Hexa correspondant au type d'équipement
var typeEq_hexa = {"sign" : "01", "feu" : "02"};

// Codes binaires correspondant aux couleurs des feux
var couleurFeu_bin = {"b" : "11", "r" : "10", "v" : "01"};


$(document).ready(function()
{
	$('#numeros').css("display", "none");
	
	$("#showIds").on("change", function(e) {
		if ($(e.target).prop("checked"))
			$('#numeros').css("display", "initial");
		else
			$('#numeros').css("display", "none");
	});
	$("#showLegend").on("change", function(e) {
		if ($(e.target).prop("checked"))
			$('#legende').css("display", "initial");
		else
			$('#legende').css("display", "none");
	});
	$("#lumiere circle").on("click", function(e)
	{
		console.log("Clic sur feu : #" + e.target.id);
		
		allumerFeu(e.target);
	});
});

/*********************/
/*** BEGIN FONCTIONS ***/
/*********************/

// Allumage d'un feu à partir d'une target *feu*, possibilité de forcer une *couleur*
var allumerFeu = function(feu, couleur) {
	couleur = couleur || false
	
	highlightFeu($(feu));
	
	// Décomposition de l'id - [1] : module, [2] : numéro, [3] : couleur
	var detailsFeu = /feu_(\d)_(\d)_([a-z])/.exec(feu.id);
	
	// Si on a forcé une couleur, on l'applique
	if (couleur) detailsFeu[3]  = couleur;
	
	// La requête permet d'identifier le feu sur le module, et lui indique la couleur à prendre
	var requete = "00000000".insertAt(detailsFeu[2] * 2, couleurFeu_bin[detailsFeu[3]]).bin2hex();
	
	// Concaténation du type d'équipement, du n° de module et de la requête
	var serialMessage = ("602" + module_hexa[detailsFeu[1]] + " " + requete);
	
	socket.emit("sendSerial", serialMessage);
}

// Toggle de la bordure du *$feu*
var highlightFeu = function($feu)
{
	// On enlève la classe selected des "voisins" du feu cliqué
	$feu.siblings("circle").removeClass("lumiere_selected");
	$feu.addClass("lumiere_selected")	
}

/* Ces fonctions ajoutent des méthodes à l'objet String natif de Javascript */

// Remplace les caractères la position *index* par *string*
String.prototype.insertAt = function(index, string)
{
	return ((index < 8)? this.substr(0, 8 - index) : "") + string + ((index > 2)? this.substr(8 - index + 2) : "");
}
// Convertit la chaine de caractères en binaire
String.prototype.bin2hex = function()
{
	return this.match(/.{4}/g).reduce(function(acc, i) {
		return acc + parseInt(i, 2).toString(16);
	}, '')
}
String.prototype.hex2bin = function (h) {
    return this.split('').reduce(function(acc, i) {
        return acc + ('000' + parseInt(i, 16).toString(2)).substr(-4, 4);
    }, '')
}

/*******************/
/** END FONCTIONS **/
/*******************/